import express from "express";
import router from "./src/routes/router";
import cors from "cors";
import IClient from "./src/storage/IClient";
import MongoClient from "./src/storage/MongoClient";
import bodyParser from "body-parser";

const clientDB: IClient = new MongoClient
clientDB.connect();

const app = express();
app.set("port", process.env.PORT || 3001);

app.use(bodyParser.json());

app.use(cors());
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Content-Range, Accept");
    res.append('Access-Control-Expose-Headers', 'Content-Range');
    next();
});

app.use("/", router);

app.listen(app.get('port'), () => {
    console.log(`Find the server at: http://localhost:${app.get('port')}/`);
});

export default app;
