import addDocument from "../../src/storage/addDocument";
import createCollection from "../../src/storage/createCollection";
import UniversalCollection from "../../src/storage/models/UniversalCollection";

const json = JSON.parse(
    "{" +
    '"№" : "1",' +
    '"Адрес" : "Улица Ленина, 24",' +
    '"Объект" : "Квартира",' +
    '"Площадь" : "100",' +
    '"Цена" : "2500000",' +
    '"%" : "20",' +
    '"Этаж" : "8",' +
    '"Доп cвед" : "Отсутствуют",' +
    '"Контакты" : "test@gmail.com",' +
    '"Описание" : "Хорошая квартира"' +
    "}",
);

describe("test", () => {
    it("test",  () => {
        const collection = createCollection("tests", UniversalCollection);
        addDocument(collection, { body: json });
        addDocument(collection, { body: json });
        addDocument(collection, { body: json });
        addDocument(collection, { body: json });
        addDocument(collection, { body: json });
        addDocument(collection, { body: json });
        addDocument(collection, { body: json });
        addDocument(collection, { body: json });
        addDocument(collection, { body: json });
        addDocument(collection, { body: json });
    });
});
