import chai from "chai";
import mongoose from "mongoose";
import addDocument from "../../storage/addDocument";
import createCollection from "../../storage/createCollection";
import getPosts from "../../storage/getPosts";

const assert = chai.assert;

const Schema = mongoose.Schema;

const collectionSchema = new Schema(
    {
        body: JSON,
    },
);

const json1 = JSON.parse('{ "test": 0, "id": 0 }');
const json2 = JSON.parse('{ "test": 1, "id": 1 }');
const json3 = JSON.parse('{ "test": 2, "id": 2 }');
const json4 = JSON.parse('{ "test": 3, "id": 3 }');
const json5 = JSON.parse('{ "test": 4, "id": 4 }');

let collection: any;

describe("getPosts tests", () => {
    before( (done) => {
        collection = createCollection("1", collectionSchema);
        addDocument(collection, { body: json1 });
        addDocument(collection, { body: json2  });
        addDocument(collection, { body: json3  });
        addDocument(collection, { body: json4  });
        addDocument(collection, { body: json5  });
        done();
    } );
    after( (done) => {
        collection.collection.drop();
        done();
    });

    it("getPosts should return page 0 with 5 elements", (done) => {
        getPosts(0, 5, "1").then((result) => {
            const data1 = Array<JSON>();

            data1.push(json1);
            data1.push(json2);
            data1.push(json3);
            data1.push(json4);
            data1.push(json5);

            const res = {
                data: data1,
                total: 5,
            };

            assert.equal(res.total, result.total, "Total is not computed correctly.");
            assert.equal(res.data.length, result.data.length, "The request did not work correctly.");
        });
        done();
    });

    it("getPosts should return page 1 with 5 elements", (done) => {
        getPosts(1, 5, "1").then( (result) => {
            const res = {
                data: Array<JSON>(),
                total: 5,
            };

            assert.equal(res.total, result.total, "Total is not computed correctly.");
            assert.equal(res.data.length, result.data.length, "The request did not work correctly.");
        });
        done();
    });

    it("getPosts should return page 1 with 2 elements", (done) => {
        getPosts(1, 2, "1").then((result) => {
            const data = Array<JSON>();

            data.push(json3);
            data.push(json4);

            const res = {
                data,
                total: 5,
            };

            assert.equal(res.total, result.total, "Total is not computed correctly.");
            assert.equal(res.data.length, result.data.length, "The request did not work correctly.");
        });
        done();
    });

    it("getPosts should return page 2 with 2 elements", (done) => {
        getPosts(2, 2, "1").then((result) => {
            const data = Array<JSON>();

            data.push(json5);

            const res = {
                data,
                total: 5,
            };

            assert.equal(res.total, result.total, "Total is not computed correctly.");
            assert.equal(res.data.length, result.data.length, "The request did not work correctly.");
        });
        done();
    });
});
