import chai from "chai";
import addDocument from "../../storage/addDocument";
import createNewCollection from "../../storage/createNewCollection";
import IClient from "../../storage/IClient";
import MongoClient from "../../storage/MongoClient";

const clientDB: IClient = new MongoClient();
clientDB.connect();

const collectionName = "TestCollection";

const rangeSearchFields = [
    { price: Number },
    { date: String },
];

let collection: any;

const assert = chai.assert;

describe("createNewCollection tests", () => {
    before( () => collection = createNewCollection(collectionName, rangeSearchFields) );
    after( (done) => {
        collection.collection.drop();
        done();
    });

    it("createNewCollection should create indexes",  (done) => {
        const indexes = collection.schema.indexes();
        assert.deepEqual(indexes[0], [ { tags: 1 }, { background: true } ], "The schema is not created correctly." );
        assert.deepEqual(indexes[1], [ { tags: "text" }, { background: true }],
        "The schema is not created correctly." );
        assert.deepEqual(indexes[2], [ { price: 1 }, { background: true } ],
             "createNewCollection don't work correctly." );
        assert.deepEqual(indexes[3], [ { date: 1 }, { background: true } ],
             "createNewCollection don't work correctly." );
        done();
    });

    it("range search request should work", (done) => {
        before( () => {
            for (let i = 1; i < 10; ++i) {
                addDocument(collection, {
                    date: "October 12",
                    price: i,
                });
            }
        });
        collection.find({
            price: { $gte: 5, $lte: 8},
        }).sort({ price: 1 })
        .then((result: any) => {
            assert.equal(5, result[0].price, "createNewCollection don't work correctly.");
            assert.equal(6, result[1].price, "createNewCollection don't work correctly.");
            assert.equal(7, result[2].price, "createNewCollection don't work correctly.");
            assert.equal(8, result[3].price, "createNewCollection don't work correctly.");
        });
        done();
    });

    it("full text search request should work", (done) => {
        addDocument(collection, {
            tags: "full text search request should work",
        })
        .then( () => {
            return collection.find({
                $text: {$search: "request"},
            });
        })
        .then( (result: any) => {
            assert.equal("full text search request should work", result[0].tags,
             "The schema is not created correctly.");
        });
        done();
    });
});
