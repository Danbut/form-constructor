const countDocuments = (model: any, document: object) => model.countDocuments(document);

export default countDocuments;
