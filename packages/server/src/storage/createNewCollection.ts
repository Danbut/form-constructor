import createCollection from "../storage/createCollection";
import UniversalCollection from "../storage/models/UniversalCollection";

interface ILooseObject {
    [key: string]: any;
}

function createNewCollection(collectionName: string, rangeSearchFields: object[]) {
    const schema = UniversalCollection;
    rangeSearchFields.forEach( (field: any) => {
        const name: string[] = Object.keys(field);
        const type = Object.values(field);
        const fieldIndex: ILooseObject = {};
        fieldIndex[(name[0])] = {
            require: false,
            type: type[0],
        };
        schema.add( fieldIndex );
        const fieldIndex1: ILooseObject = {};
        fieldIndex1[(name[0])] = 1;
        schema.index(fieldIndex1);
    });
    return createCollection(collectionName, schema);
}

export default createNewCollection;
