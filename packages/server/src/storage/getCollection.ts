import mongoose from "mongoose";

const getCollection = (collectionsName: string) => mongoose.connection.collection(collectionsName);

export default getCollection;
