import mongoose from "mongoose";
import IClient from "./IClient";

const dbUri = "mongodb://localhost:27017/TestDatabase";

const options = {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

class MongoClient implements IClient {
    /**
     * This method is responsible for connecting to MongoDB.
     * @returns Connection or error.
     */
    public connect() {
        mongoose.connect(dbUri, options).then().catch((err) => {
            throw err;
        });
    }
    /**
     * This method is responsible for disconnecting to MongoDB.
     * @param connection Connection
     */
    public disconnect() {
        mongoose.connection.close().catch((err) => {
            throw err;
        });
    }
}

export default MongoClient;
