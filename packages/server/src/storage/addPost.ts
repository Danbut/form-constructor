import countDocuments from "./countDocuments";
import getCollection from "./getCollection";

const addPost = async (collectionsName: string, post: any) => {
    const collection = await getCollection(collectionsName);
    if (!collection) { throw Error("Collection doen't exist."); }
    const anyDocument = await collection.findOne({});
    const collectionsKeys = Object.keys(anyDocument);
    post.Площадь = post["Общая площадь"];
    post.Объект = post["Тип объекта недвижимости"];
    post["Тип сделки"] = post["Тип объявления"];
    const postKeys = Object.keys(post);
    const document = Object.create({});
    collectionsKeys.forEach( (key) => {
        if (postKeys.includes(key)) {
            document[key] = post[key];
        }
        if (postKeys.includes("Цена")) {
            document["Цена за квадратный метр"] = Number(document.Цена) / Number(document.Площадь);
        }
        document.body = post;
        document.tags = [];
    });
    await collection.insertOne(document);
    return await countDocuments(collection, {});
};

export default addPost;
