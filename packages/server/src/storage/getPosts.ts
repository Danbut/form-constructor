import countDocuments from "./countDocuments";
import getCollection from "./getCollection";

async function getPosts(pageNum: number, perPage: number, collectionName: string) {
    try {
        const result = {
            data: Array<JSON>(),
            total: 0,
        };
        const collection = getCollection(collectionName);

        const total = await countDocuments(collection, {});
        result.total = total;

        let id = perPage * pageNum;
        if (collection) { await collection
        .find()
        .project({ _id: 0, __v: 0 })
        .limit(Number(perPage))
        .skip(Number(perPage * pageNum))
        .forEach( (document: any) => {
            if (document.body !== undefined) {
                document.body.id = id;
                result.data.push(document.body);
                ++id;
            }
        });
        }

        return result;
    } catch (err) {
        throw new Error("Collection " + collectionName + " doesn't exist.");
    }
}

export default getPosts;
