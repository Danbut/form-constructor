const addDocument = (model: any, document: object) => model.create(document);

export default addDocument;
