interface IClient {
    connect(): void;
    disconnect(): void;
}

export default IClient;
