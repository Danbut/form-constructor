import mongoose from "mongoose";

const createCollection = (collectionName: string, collectionSchema: mongoose.Schema) => (
    mongoose.model(collectionName, collectionSchema)
);

export default createCollection;
