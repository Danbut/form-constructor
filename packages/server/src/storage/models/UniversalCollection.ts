import mongoose from "mongoose";

const Schema = mongoose.Schema;

const UniversalCollection = new Schema(
    {
        body: {
            require: true,
            type: JSON,
        },
        tags: {
            text: true,
            type: [String],
        },
    },
);

UniversalCollection.index({tags: "text"});

UniversalCollection.set("autoIndex", true);

export default UniversalCollection;
