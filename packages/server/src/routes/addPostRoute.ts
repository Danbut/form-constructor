import express from "express";
import addPost from "../storage/addPost";

const router = express.Router();

router.post("/posts", (req, res) => (
    addPost(req.body.collectionsName, req.body.data)
    .then((result: any) => {
        return res.status(200).send({ data: { id: result } });
    })
));

export = router;
