import express from "express";
import getPosts from "../storage/getPosts";

const router = express.Router();

router.get("/posts", (req, res) => {
    getPosts(req.query.page, req.query.perPage, req.query.collectionsName).then( (result) =>
        res.status(200).json(result),
    );
});

export = router;
