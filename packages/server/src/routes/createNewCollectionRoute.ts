import express from "express";
import createNewCollection from "../storage/createNewCollection";

const router = express.Router();

router.post("/newCollection", (req, res) => {
    res.status(200).send(createNewCollection(req.body.collectionName, req.body.rangeSearchFields));
});

export = router;
