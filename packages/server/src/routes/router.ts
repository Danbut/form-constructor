import express from "express";
import addPostRoute from "./addPostRoute";
import createNewCollectionRoute from "./createNewCollectionRoute";
import getPostsRoute from "./getPostsRoute";

const router = express.Router();

router.use("/", createNewCollectionRoute);
router.use("/", getPostsRoute);
router.use("/", addPostRoute);

export = router;
