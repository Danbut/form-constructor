import axios from "axios";
// @ts-ignore
import { CREATE, GET_LIST } from "react-admin";

export default (type: any, resource: any, params: any, collectionsName?: string) => {
    let res: any;
    switch (type) {
        case GET_LIST: {
            const { page, perPage }  = params.pagination;
            res = axios.get("/posts", {
                params: {
                    collectionsName,
                    page: page - 1,
                    perPage,
                },
            }).then((response) => {
                return {
                    data: response.data.data,
                    total: response.data.total,
                };
            });
            break;
        }
        case CREATE: {
            res = axios.post("/posts", {
                collectionsName,
                data: params.data,
            }).then( (response) => {
                return {
                    data: response.data.data,
                };
            });
            break;
        }

    }
    return res;
};
