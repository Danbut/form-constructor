import React from "react";
// @ts-ignore
import { Admin, Resource } from "react-admin";
import UnitCreate from "./CreateForm/UnitCreate";
import postsDataProvider from "./postsDataProvider";
import PostsList from "./PostsList";

const PostsCabinet = (props: any) => {
    const { collectionName } = { collectionName: props.match.params.collectionName };

    const dataProvider = (type: any, resource: any, params: any) => (
        postsDataProvider(type, resource, params, collectionName)
    );

    return (
        <Admin
            dataProvider={dataProvider}
        >
        <Resource
            options={{ label: `${collectionName}` }}
            name="Posts"
            list={PostsList}
            create={UnitCreate}
        />
        </Admin>
    );
};

export default PostsCabinet;
