import React from "react";
// @ts-ignore
import { Datagrid, List, TextField } from "react-admin";

const PostsList = (props: any) => {
    return (
        <List {...props}>
            <Datagrid>
                <TextField source="№" />
                <TextField source="Адрес" />
                <TextField source="Объект" />
                <TextField source="Площадь" />
                <TextField source="Цена" />
                <TextField source="%" />
                <TextField source="Этаж" />
                <TextField source="Доп cвед" />
                <TextField source="Контакты" />
                <TextField source="Описание" />
            </Datagrid>
        </List>
    );
};

export default PostsList;
