import React from "react";

export const Paragraph = (props: any) => {
    const { text } = props;
    return (
        <p>{`${text}`}</p>
    );
};
