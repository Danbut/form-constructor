import React from "react";
// @ts-ignore
import { CheckboxGroupInput } from "react-admin";

export const CheckboxGroup = (props: any) => {
    const { source, choices, validate, onChange } = props;
    return (
        <CheckboxGroupInput
            onChange={onChange}
            source={source}
            choices={choices}
            validate={validate}
            options={{fullWidth: true}}
        />
    );
};
