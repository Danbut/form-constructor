import React from "react";
// @ts-ignore
import { TextInput } from "react-admin";

export const Text = (props: any) => {
    const { source, validate, text, onChange } = props;
    if (text) { return (
        <div>
            <TextInput
                onChange={onChange}
                validate={validate}
                source={source}
            />
            {`${text}`}
        </div>
    );
    } else { return (
        <TextInput
            onChange={onChange}
            validate={validate}
            source={source}
        />
    );
    }
};
