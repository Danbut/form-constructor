import React from "react";
// @ts-ignore
import { SelectInput } from "react-admin";

export const Select = (props: any) => {
    const { source, choices, validate, onChange } = props;
    return (
        <SelectInput
            onChange={onChange}
            source={source}
            choices={choices}
            validate={validate}
            options={{fullWidth: true}}
        />
    );
};
