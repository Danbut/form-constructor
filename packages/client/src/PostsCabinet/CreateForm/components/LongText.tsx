import React from "react";
// @ts-ignore
import { LongTextInput } from "react-admin";

export const LongText = (props: any) => {
    const { source, validate, onChange } = props;
    return (
        <LongTextInput
            onChange={onChange}
            options={{fullWidth: true}}
            validate={validate}
            source={source}
        />
    );
};
