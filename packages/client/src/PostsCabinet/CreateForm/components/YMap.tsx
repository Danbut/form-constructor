import React from "react";
// @ts-ignore
import { Map, YMaps } from "react-yandex-maps";

export const YMap = (props: any) => {
    return (
        <YMaps>
        <Map defaultState={{ center: [55.75, 37.57], zoom: 9 }} />
        </YMaps>
    );
};
