import React from "react";
// @ts-ignore
import { BooleanInput } from "react-admin";

export const Switch = (props: any) => {
    const { source, validate, text, onChange } = props;
    if (text) { return (
        <div>
            <BooleanInput
                onChange={onChange}
                validate={validate}
                source={source}
            />
            {`${text}`}
        </div>
    );
    } else { return (
        <BooleanInput
            onChange={onChange}
            validate={validate}
            source={source}
        />
    );
    }
};
