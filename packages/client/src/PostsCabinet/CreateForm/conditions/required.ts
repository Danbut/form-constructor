const required = (
    args: [{
            type: string,
            name?: string,
            value?: string,
            data?: object[],
            },
            Array<{ name: string, source: string, value: string }>
          ],
    ) => {
    const conditions = args[0];
    const currentForm =  args[1];
    const name =  conditions.name;
    const value =  conditions.value;
    let result = false;

    if (value) {
        const exist = currentForm.find((element: { name: string; value: string; }) => {
            return element.name === name && element.value === value;
        });

        if (exist) { result = true; }
    } else {
        currentForm.forEach( (element: { name: string; value: string; }) => {
            if (element.name === name) { result = true; }
        });
    }

    return result;
};

export default required;
