import { conditions } from "../dictionaries/conditions";

const or = (
    args: [{
            type: string,
            name?: string,
            value?: string,
            data?: object[],
            },
            Array<{ name: string, source: string, value: string }>
          ],
    ) => {
    const elementConditions = args[0];
    const currentForm =  args[1];
    if (elementConditions.data) {
        const data = elementConditions.data;
        const result = Array<boolean>();

        data.forEach( (operand: any) => {
            const condition = conditions.get(operand.type)!;
            result.push(condition([ operand, currentForm ]));
        });

        if (result[0] || result[1]) { return true; } else { return false; }
    } else { return false; }
};

export default or;
