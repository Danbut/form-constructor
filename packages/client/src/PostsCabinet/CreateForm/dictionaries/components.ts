import { ReactNode } from "react";
import { CheckboxGroup } from "../components/CheckboxGroup";
import { LongText } from "../components/LongText";
import { Paragraph } from "../components/Paragraph";
import { Select } from "../components/Select";
import { Switch } from "../components/Switch";
import { Text } from "../components/Text";
import { YMap } from "../components/YMap";

export const components = new Map<string, (props: any) => ReactNode>();

components.set("Select", Select);
components.set("LongText", LongText);
components.set("Paragraph", Paragraph);
components.set("Text", Text);
components.set("YMap", YMap);
components.set("Switch", Switch);
components.set("CheckboxGroup", CheckboxGroup);
