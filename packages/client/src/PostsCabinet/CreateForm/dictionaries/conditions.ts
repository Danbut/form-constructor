import and from "../conditions/and";
import or from "../conditions/or";
import required from "../conditions/required";

export const conditions = new Map<string, (
    args: [{
            type: string,
            name?: string,
            value?: string,
            data?: object[],
            },
            Array<{ name: string, source: string, value: string }>
          ],
    ) => boolean>();

conditions.set("required", required);
conditions.set("and", and);
conditions.set("or", or);
