// @ts-ignore
import { number, required } from "react-admin";

export const validators = new Map<string, any>();

validators.set("required", required());
validators.set("number", number());
