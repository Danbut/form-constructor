import React, { Component, ReactNode } from "react";
// @ts-ignore
import { Create, SimpleForm } from "react-admin";
import { components } from "./dictionaries/components";
import { conditions } from "./dictionaries/conditions";
import { schemes } from "./dictionaries/schemes";
import { validators } from "./dictionaries/validators";

const initialForm = new Array<{ name: string, source: string, value: string }>();
initialForm.push({ name: "AccountType", source: "Тип аккаунта", value: "" });

class UnitCreate extends Component {
    public state = {
        currentForm: initialForm,
    };

    public buildForm = (result: boolean, schema: any,
                        currentForm: Array<{ name: string, source: string, value: string }>) => {
        if (result) {
            const exist = currentForm.find( (element) => element.source === schema.source );

            if (!exist) { currentForm.push({ name: schema.name, source: schema.source, value: ""}); }

            currentForm.forEach( (element) => {
                if (element.name !== schema.name && element.source === schema.source) {
                    element.name = schema.name;
                }
            });
        } else { currentForm = currentForm.filter( (element) => element.name !== schema.name); }

        return currentForm;
    }

    public checkElementsRenderingConditions = (currentForm: Array<{ name: string, source: string, value: string }>) => {
        schemes.forEach( (schema) => {
            let result = false;
            if (schema.hasOwnProperty("conditions") && schema.conditions) {
                const condition = conditions.get(schema.conditions.type)!;
                result = condition([ schema.conditions, currentForm ]);
                currentForm = this.buildForm(result, schema, currentForm);
            } else { currentForm = this.buildForm(true, schema, currentForm); }
        });
        return currentForm;
    }

    public setForm = (currentForm: Array<{ name: string, source: string, value: string }>) => {
        let modifiedForm = new Array<{ name: string, source: string, value: string }>();

        while (JSON.stringify(currentForm) !== JSON.stringify(modifiedForm)) {
            if (modifiedForm.length) {
                currentForm = modifiedForm;
            } else {
                modifiedForm = currentForm;
            }

            modifiedForm = this.checkElementsRenderingConditions(modifiedForm);
        }

        this.setState({ currentForm });
    }

    public changeCurrentForm = (changes: { name: string, source: string, value: string }) => {
        const { currentForm } = this.state;

        currentForm.forEach( (element) => {
            if (element.name === changes.name) { element.value = changes.value; }
        });

        this.setForm(currentForm);
    }

    public render() {
        const form = Array<ReactNode>();
        const { currentForm } = this.state;

        currentForm.forEach( (element) => {
            const schema = schemes.find( (result) => result.name === element.name )!;
            const validate = Array<any>();
            const component = components.get(schema.type)!;

            if (schema.hasOwnProperty("validation")) { schema.validation!.forEach((validator: any) => {
                    validate.push(validators.get(validator));
                });
            }

            const onChange = (event: object, value: string) => (
                this.changeCurrentForm({ name: schema.name, source: schema.source!, value })
            );

            form.push(component({ ...schema, validate, onChange }));
        });

        return (
            <Create {...this.props}>
                <SimpleForm>
                    {form}
                </SimpleForm>
            </Create>
        );
    }
}

export default UnitCreate;
