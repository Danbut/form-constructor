export interface IScheme {
    choices?: object[];
    conditions?: {
        type: string,
        name?: string,
        value?: string,
        data?: object[],
    };
    name: string;
    source?: string;
    text?: string;
    type: string;
    validation?: string[];
}
