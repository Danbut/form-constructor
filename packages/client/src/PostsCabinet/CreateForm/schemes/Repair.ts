import { IScheme } from "../interfaces/IScheme";

const Repair: IScheme = {
    choices: [
        { id: "Косметический", name: "Косметический"},
        { id: "Евро", name: "Евро"},
        { id: "Дизайнерский", name: "Дизайнерский"},
        { id: "Без ремонта", name: "Без ремонта"},
    ],
    conditions: {
        name: "KitchenArea",
        type: "required",
    },
    name: "Repair",
    source: "Ремонт",
    type: "Select",
};

export default Repair;
