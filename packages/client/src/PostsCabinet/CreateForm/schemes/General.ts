import { IScheme } from "../interfaces/IScheme";

export const General: IScheme = {
    choices: [
        { id: "Окна во двор", name: "Окна во двор"},
        { id: "Окна на улицу", name: "Окна на улицу"},
        { id: "Балкон", name: "Балкон"},
        { id: "Лоджия", name: "Лоджия"},
    ],
    conditions: {
        name: "Additionally",
        type: "required",
    },
    name: "General",
    source: "Общее",
    type: "CheckboxGroup",
};

export default General;
