import { IScheme } from "../interfaces/IScheme";

const YMap: IScheme = {
    conditions: {
        name: "Address",
        type: "required",
    },
    name: "YMap",
    source: "YMap",
    type: "YMap",
};

export default YMap;
