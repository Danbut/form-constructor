import { IScheme } from "../interfaces/IScheme";

const GarbageChute: IScheme = {
    conditions: {
        name: "Name",
        type: "required",
    },
    name: "GarbageChutet",
    source: "Мусоропровод",
    type: "Switch",
};

export default GarbageChute;
