import { IScheme } from "../interfaces/IScheme";

const PledgeToOwner: IScheme = {
    conditions: {
        data: [
            {
                name: "PriceAndTerms",
                type: "required",
            },
            {
                name: "LivingRentalObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "PledgeToOwner",
    source: "Залог",
    type: "Text",
    validation: ["required", "number"],
};

export default PledgeToOwner;
