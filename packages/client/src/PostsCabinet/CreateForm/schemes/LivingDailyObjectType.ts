import { IScheme } from "../interfaces/IScheme";

const LivingDailyObjectType: IScheme = {
    choices: [
        { id: "Квартира", name: "Квартира" },
        { id: "Комната", name: "Комната" },
        { id: "Дом", name: "Дом" },
        { id: "Койко-место", name: "Койко-место" },
    ],
    conditions: {
        data: [
            {
                name: "RentalType",
                type: "required",
                value: "Посуточно",
            },
            {
                name: "DisabledCommerceImmovablesType",
                type: "required",
            },
        ],
        type: "and",
    },
    name: "LivingDailyObjectType",
    source: "Тип объекта недвижимости",
    type: "Select",
    validation: ["required"],
};

export default LivingDailyObjectType;
