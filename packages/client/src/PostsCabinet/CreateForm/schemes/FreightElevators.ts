import { IScheme } from "../interfaces/IScheme";

const FreightElevators: IScheme = {
    choices: [
        { id: "нет", name: "нет"},
        { id: "1", name: "1"},
        { id: "2", name: "2"},
        { id: "3", name: "3"},
        { id: "4", name: "4"},
    ],
    conditions: {
        name: "AboutBuilding",
        type: "required",
    },
    name: "FreightElevators",
    source: "Грузовых лифтов",
    type: "Select",
};

export default FreightElevators;
