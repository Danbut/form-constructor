import { IScheme } from "../interfaces/IScheme";

const CeilingHeight: IScheme = {
    conditions: {
        name: "Name",
        type: "required",
    },
    name: "CeilingHeight",
    source: "Высота потолков",
    type: "Text",
    validation: ["number"],
};

export default CeilingHeight;
