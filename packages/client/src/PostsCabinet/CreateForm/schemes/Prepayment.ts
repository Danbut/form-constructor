import { IScheme } from "../interfaces/IScheme";

const Prepayments: IScheme = {
    choices: [
        { id: "1 месяц", name: "1 месяц"},
        { id: "2 месяца", name: "2 месяца"},
        { id: "3 месяца", name: "3 месяца"},
        { id: "4 месяца", name: "4 месяца"},
        { id: "5 месяцев", name: "5 месяцев"},
        { id: "6 месяцев", name: "6 месяцев"},
        { id: "7 месяцев", name: "7 месяцев"},
        { id: "8 месяцев", name: "8 месяцев"},
        { id: "9 месяцев", name: "9 месяцев"},
        { id: "10 месяцев", name: "10 месяцев"},
        { id: "11 месяцев", name: "11 месяцев"},
        { id: "1 год", name: "1 год"},
    ],
    conditions: {
        data: [
            {
                name: "PriceAndTerms",
                type: "required",
            },
            {
                name: "LivingRentalObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "Prepayments",
    source: "Предоплата",
    type: "Select",
};

export default Prepayments;
