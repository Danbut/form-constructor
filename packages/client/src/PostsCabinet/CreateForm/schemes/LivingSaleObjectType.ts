import { IScheme } from "../interfaces/IScheme";

const LivingSaleObjectType: IScheme = {
    choices: [
        { id: "Квартира", name: "Квартира" },
        { id: "Комната", name: "Комната" },
        { id: "Койко-место", name: "Койко-место" },
        { id: "Дом/Дача", name: "Дом/Дача" },
        { id: "Коттедж", name: "Коттедж" },
        { id: "Таунхаус", name: "Таунхаус" },
        { id: "Часть дома", name: "Часть дома" },
        { id: "Участок", name: "Участок" },
    ],
    conditions: {
        data: [
            {
                name: "AdType",
                type: "required",
                value: "Продажа",
            },
            {
                name: "ImmovablesType",
                type: "required",
                value: "Жилая",
            },
        ],
        type: "and",
    },
    name: "LivingSaleObjectType",
    source: "Тип объекта недвижимости",
    type: "Select",
    validation: ["required"],
};

export default LivingSaleObjectType;
