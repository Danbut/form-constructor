import { IScheme } from "../interfaces/IScheme";

const Name: IScheme = {
    conditions: {
        name: "FreightElevators",
        type: "required",
    },
    name: "Name",
    source: "Название",
    type: "Text",
};

export default Name;
