import { IScheme } from "../interfaces/IScheme";

const AdType: IScheme = {
    choices: [
        { id: "Продажа", name: "Продажа" },
        { id: "Аренда", name: "Аренда" },
    ],
    conditions: {
        name: "AccountType",
        type: "required",
    },
    name: "AdType",
    source: "Тип объявления",
    type: "Select",
    validation: ["required"],
};

export default AdType;
