import { IScheme } from "../interfaces/IScheme";

const Floor: IScheme = {
    conditions: {
        name: "AboutObject",
        type: "required",
    },
    name: "Floor",
    source: "Этаж",
    type: "Text",
    validation: ["required", "number"],
};

export default Floor;
