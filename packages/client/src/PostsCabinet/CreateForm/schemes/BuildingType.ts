import { IScheme } from "../interfaces/IScheme";

const BuildingType: IScheme = {
    choices: [
        { id: "Кирпичный", name: "Кирпичный"},
        { id: "Монолитный", name: "Монолитный"},
        { id: "Панельный", name: "Панельный"},
        { id: "Блочный", name: "Блочный"},
        { id: "Деревянный", name: "Деревянный"},
        { id: "Монолитно-кирпичный", name: "Монолитно-кирпичный"},
        { id: "Сталинский", name: "Сталинский"},
    ],
    conditions: {
        name: "AboutBuilding",
        type: "required",
    },
    name: "BuildingType",
    source: "Тип здания",
    type: "Select",
};

export default BuildingType;
