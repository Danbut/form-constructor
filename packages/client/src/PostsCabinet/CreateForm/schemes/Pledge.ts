import { IScheme } from "../interfaces/IScheme";

const Pledge: IScheme = {
    choices: [
        { id: "Без залога", name: "Без залога"},
    ],
    conditions: {
        data: [
            {
                name: "PriceAndTerms",
                type: "required",
            },
            {
                name: "LivingRentalObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "Pledge",
    source: "Залог собственнику",
    type: "CheckboxGroup",
    validation: ["required", "number"],
};

export default Pledge;
