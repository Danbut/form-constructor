import { IScheme } from "../interfaces/IScheme";

const Address: IScheme = {
    conditions: {
        data: [
            {
                name: "LivingSaleObjectType",
                type: "required",
                value: "Квартира",
            },
            {
                name: "LivingRentalObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "or",
    },
    name: "Address",
    source: "Адрес",
    type: "LongText",
    validation: ["required"],
};

export default Address;
