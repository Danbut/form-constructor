import { IScheme } from "../interfaces/IScheme";

const TotalArea: IScheme = {
    conditions: {
        name: "AboutObject",
        type: "required",
    },
    name: "TotalArea",
    source: "Общая площадь",
    text: "м\u00b2",
    type: "Text",
    validation: ["required", "number"],
};

export default TotalArea;
