import { IScheme } from "../interfaces/IScheme";

const Parking: IScheme = {
    choices: [
        { id: "Наземная", name: "Наземная"},
        { id: "Многоуровневая", name: "Многоуровневая"},
        { id: "Подземная", name: "Подземная"},
        { id: "На крыше", name: "На крыше"},
    ],
    conditions: {
        name: "Name",
        type: "required",
    },
    name: "Parking",
    source: "Парковка",
    type: "Select",
};

export default Parking;
