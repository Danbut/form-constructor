import { IScheme } from "../interfaces/IScheme";

const NumberOfRooms: IScheme = {
    choices: [
        { id: "1", name: "1"},
        { id: "2", name: "2"},
        { id: "3", name: "3"},
        { id: "4", name: "4"},
        { id: "5", name: "5"},
        { id: "6", name: "6 и более"},
        { id: "free_plan", name: "Свободная планировка"},
        { id: "studio", name: "Студия"},
    ],
    conditions: {
        name: "AboutObject",
        type: "required",
    },
    name: "NumberOfRooms",
    source: "Количество комнат",
    type: "Select",
    validation: ["required"],
};

export default NumberOfRooms;
