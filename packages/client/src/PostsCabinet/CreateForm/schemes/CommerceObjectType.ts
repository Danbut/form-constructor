import { IScheme } from "../interfaces/IScheme";

const CommerceObjectType: IScheme = {
    choices: [
        { id: "Офис", name: "Офис" },
        { id: "Здание", name: "Здание" },
        { id: "Торговая площадь", name: "Торговая площадь" },
        { id: "Помещение свободного назначения", name: "Помещение свободного назначения" },
        { id: "Производство", name: "Производство" },
        { id: "Склад", name: "Склад" },
        { id: "Гараж", name: "Гараж" },
        { id: "Готовый бизне", name: "Готовый бизнес" },
        { id: "Коммерческая земля", name: "Коммерческая земля" },
    ],
    conditions: {
        name: "ImmovablesType",
        type: "required",
        value: "Коммерческая",
    },
    name: "CommerceObjectType",
    source: "Тип объекта недвижимости",
    type: "Select",
    validation: ["required"],
};

export default CommerceObjectType;
