import { IScheme } from "../interfaces/IScheme";

const RentalType: IScheme = {
    choices: [
        { id: "Длительно", name: "Длительно"},
        { id: "Несколько месяцев", name: "Несколько месяцев"},
        { id: "Посуточно", name: "Посуточно"},
    ],
    conditions: {
        name: "AdType",
        type: "required",
        value: "Аренда",
    },
    name: "RentalType",
    source: "Тип аренды",
    type: "Select",
    validation: ["required"],
};

export default RentalType;
