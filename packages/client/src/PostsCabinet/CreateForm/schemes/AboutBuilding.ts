import { IScheme } from "../interfaces/IScheme";

const AboutBuilding: IScheme = {
    conditions: {
        name: "General",
        type: "required",
    },
    name: "AboutBuilding",
    source: "AboutBuilding",
    text: "О здании",
    type: "Paragraph",
};

export default AboutBuilding;
