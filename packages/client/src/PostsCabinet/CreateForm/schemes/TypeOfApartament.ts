import { IScheme } from "../interfaces/IScheme";

const TypeOfApartament: IScheme = {
    choices: [
        { id: "Квартира", name: "Квартира"},
        { id: "Апартаменты", name: "Апартаменты"},
    ],
    conditions: {
        name: "AboutObject",
        type: "required",
    },
    name: "TypeOfApartament",
    source: "Тип жилья",
    type: "Select",
    validation: ["required"],
};

export default TypeOfApartament;
