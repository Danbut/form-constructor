import { IScheme } from "../interfaces/IScheme";

const Rent: IScheme = {
    conditions: {
        data: [
            {
                name: "PriceAndTerms",
                type: "required",
            },
            {
                name: "LivingRentalObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "Rent",
    source: "Арендная плата",
    type: "Text",
    validation: ["required", "number"],
};

export default Rent;
