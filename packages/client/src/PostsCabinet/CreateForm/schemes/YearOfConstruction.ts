import { IScheme } from "../interfaces/IScheme";

const YearOfConstruction: IScheme = {
    conditions: {
        name: "Name",
        type: "required",
    },
    name: "YearOfConstruction",
    source: "Год постройки",
    type: "Text",
    validation: [ "number"],
};

export default YearOfConstruction;
