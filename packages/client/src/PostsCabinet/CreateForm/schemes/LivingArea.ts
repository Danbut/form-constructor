import { IScheme } from "../interfaces/IScheme";

const LivingArea: IScheme = {
    conditions: {
        name: "AboutObject",
        type: "required",
    },
    name: "LivingArea",
    source: "Жилая",
    text: "м\u00b2",
    type: "Text",
    validation: [ "number"],
};

export default LivingArea;
