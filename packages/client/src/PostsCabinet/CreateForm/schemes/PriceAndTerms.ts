import { IScheme } from "../interfaces/IScheme";

const PriceAndTerms: IScheme = {
    conditions: {
        name: "Parking",
        type: "required",
    },
    name: "PriceAndTerms",
    source: "PriceAndTerms",
    text: "Цена и условия сделки",
    type: "Paragraph",
};

export default PriceAndTerms;
