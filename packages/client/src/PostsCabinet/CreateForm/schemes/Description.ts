import { IScheme } from "../interfaces/IScheme";

const Description: IScheme = {
    conditions: {
        name: "Name",
        type: "required",
    },
    name: "Description",
    source: "Описание",
    type: "LongText",
};

export default Description;
