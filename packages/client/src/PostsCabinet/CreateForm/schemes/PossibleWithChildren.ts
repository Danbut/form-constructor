import { IScheme } from "../interfaces/IScheme";

const PossibleWithChildren: IScheme = {
    conditions: {
        data: [
            {
                name: "Repair",
                type: "required",
            },
            {
                name: "LivingRentalObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "PossibleWithChildren",
    source: "Можно с детьми",
    type: "Switch",
};

export default PossibleWithChildren;
