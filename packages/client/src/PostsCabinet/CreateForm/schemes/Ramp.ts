import { IScheme } from "../interfaces/IScheme";

const Ramp: IScheme = {
    choices: [
        { id: "есть", name: "есть"},
        { id: "нет", name: "нет"},
    ],
    conditions: {
        name: "Name",
        type: "required",
    },
    name: "Ramp",
    source: "Пандус",
    type: "Select",
};

export default Ramp;
