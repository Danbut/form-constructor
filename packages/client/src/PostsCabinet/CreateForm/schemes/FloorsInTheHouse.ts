import { IScheme } from "../interfaces/IScheme";

const FloorsInTheHouse: IScheme = {
    conditions: {
        name: "AboutObject",
        type: "required",
    },
    name: "FloorsInTheHouse",
    source: "Этажей в доме",
    type: "Text",
    validation: ["required", "number"],
};

export default FloorsInTheHouse;
