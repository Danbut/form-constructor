import { IScheme } from "../interfaces/IScheme";

const AboutObject: IScheme = {
    conditions: {
        name: "YMap",
        type: "required",
    },
    name: "AboutObject",
    source: "AboutObject",
    text: "Об объекте",
    type: "Paragraph",
};

export default AboutObject;
