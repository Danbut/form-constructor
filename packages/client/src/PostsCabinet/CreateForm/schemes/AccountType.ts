import { IScheme } from "../interfaces/IScheme";

const AccountType: IScheme = {
    choices: [
        { id: "Собственник", name: "Собственник" },
        { id: "Агент", name: "Агент" },
    ],
    name: "AccountType",
    source: "Тип аккаунта",
    type: "Select",
    validation: ["required"],
};

export default AccountType;
