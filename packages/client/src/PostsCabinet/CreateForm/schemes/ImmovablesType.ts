import { IScheme } from "../interfaces/IScheme";

const ImmovablesType: IScheme = {
    choices: [
        { id: "Жилая", name: "Жилая" },
        { id: "Коммерческая", name: "Коммерческая" },
    ],
    conditions: {
        data: [
            {
                name: "AdType",
                type: "required",
                value: "Продажа",
            },
            {
                data: [
                    {
                        name: "RentalType",
                        type: "required",
                        value: "Длительно",
                    },
                    {
                        name: "RentalType",
                        type: "required",
                        value: "Несколько месяцев",
                    },
                ],
                type: "or",
            },
        ],
        type: "or",
    },
    name: "ImmovablesType",
    source: "Тип недвижимости",
    type: "Select",
    validation: ["required"],
};

export default ImmovablesType;
