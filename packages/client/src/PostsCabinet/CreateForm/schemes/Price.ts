import { IScheme } from "../interfaces/IScheme";

const Price: IScheme = {
    conditions: {
        data: [
            {
                name: "PriceAndTerms",
                type: "required",
            },
            {
                name: "LivingSaleObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "Price",
    source: "Цена",
    type: "Text",
    validation: ["required", "number"],
};

export default Price;
