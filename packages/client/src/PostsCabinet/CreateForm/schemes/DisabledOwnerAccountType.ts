import { IScheme } from "../interfaces/IScheme";

const DisabledOwnerAccountType: IScheme = {
    choices: [
        { id: "Агент", name: "Агент" },
        { id: "Собственник", name: "Собственник", disabled: true },
    ],
    conditions: {
        name: "ImmovablesType",
        type: "required",
        value: "Коммерческая",
    },
    name: "DisabledOwnerAccountType",
    source: "Тип аккаунта",
    type: "Select",
    validation: ["required"],
};

export default DisabledOwnerAccountType;
