import { IScheme } from "../interfaces/IScheme";

const KitchenArea: IScheme = {
    conditions: {
        name: "AboutObject",
        type: "required",
    },
    name: "KitchenArea",
    source: "Кухня",
    text: "м\u00b2",
    type: "Text",
    validation: [ "number"],
};

export default KitchenArea;
