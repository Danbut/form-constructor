import { IScheme } from "../interfaces/IScheme";

const Additionally: IScheme = {
    conditions: {
        name: "Repair",
        type: "required",
    },
    name: "Additionally",
    source: "Additionally",
    text: "Дополнительно",
    type: "Paragraph",
};

export default Additionally;
