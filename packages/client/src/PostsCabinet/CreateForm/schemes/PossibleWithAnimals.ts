import { IScheme } from "../interfaces/IScheme";

const PossibleWithAnimals: IScheme = {
    conditions: {
        data: [
            {
                name: "Repair",
                type: "required",
            },
            {
                name: "LivingRentalObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "PossibleWithAnimals",
    source: "Можно с животными",
    type: "Switch",
};

export default PossibleWithAnimals;
