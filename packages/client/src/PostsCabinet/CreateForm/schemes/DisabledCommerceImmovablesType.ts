import { IScheme } from "../interfaces/IScheme";

const DisabledCommerceImmovablesType: IScheme = {
    choices: [
        { id: "Жилая", name: "Жилая" },
        { id: "Коммерческая", name: "Коммерческая", disabled: true },
    ],
    conditions: {
        name: "RentalType",
        type: "required",
        value: "Посуточно",
    },
    name: "DisabledCommerceImmovablesType",
    source: "Тип недвижимости",
    type: "Select",
    validation: ["required"],
};

export default DisabledCommerceImmovablesType;
