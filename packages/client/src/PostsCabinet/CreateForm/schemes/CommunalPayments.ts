import { IScheme } from "../interfaces/IScheme";

const CommunalPayments: IScheme = {
    choices: [
        { id: "включены", name: "включены"},
        { id: "не включены", name: "не включены"},
    ],
    conditions: {
        data: [
            {
                name: "PriceAndTerms",
                type: "required",
            },
            {
                name: "LivingRentalObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "CommunalPayments",
    source: "Коммунальные платежи",
    type: "Select",
};

export default CommunalPayments;
