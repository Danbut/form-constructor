import { IScheme } from "../interfaces/IScheme";

const Technics: IScheme = {
    choices: [
        { id: "Холодильник", name: "Холодильник"},
        { id: "Посудомоечная машина", name: "Посудомоечная машина"},
        { id: "Стиральная машина", name: "Стиральная машина"},
        { id: "Телевизор", name: "Телевизор"},
        { id: "Телефон", name: "Телефон"},
    ],
    conditions: {
        data: [
            {
                name: "Additionally",
                type: "required",
            },
            {
                name: "LivingRentalObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "Technics",
    source: "Техника",
    type: "CheckboxGroup",
};

export default Technics;
