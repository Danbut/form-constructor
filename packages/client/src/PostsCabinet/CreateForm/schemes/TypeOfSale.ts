import { IScheme } from "../interfaces/IScheme";

const TypeOfSale: IScheme = {
    choices: [
        { id: "Свободная продажа", name: "Свободная продажа"},
        { id: "Альтернатива", name: "Альтернатива"},
    ],
    conditions: {
        data: [
            {
                name: "PriceAndTerms",
                type: "required",
            },
            {
                name: "LivingSaleObjectType",
                type: "required",
                value: "Квартира",
            },
        ],
        type: "and",
    },
    name: "TypeOfSale",
    source: "Тип продажи",
    type: "Select",
};

export default TypeOfSale;
