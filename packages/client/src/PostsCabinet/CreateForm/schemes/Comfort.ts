import { IScheme } from "../interfaces/IScheme";

const Comfort: IScheme = {
    choices: [
        { id: "Раздельный санузел", name: "Раздельный санузел"},
        { id: "Совмещённые санузел", name: "Совмещённые санузел"},
    ],
    conditions: {
        name: "Additionally",
        type: "required",
    },
    name: "Comfort",
    source: "Комфорт",
    type: "CheckboxGroup",
};

export default Comfort;
