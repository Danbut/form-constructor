import React from "react";
import { HashRouter, Redirect, Route, Switch } from "react-router-dom";
import PostsCabinet from "./PostsCabinet/PostsCabinet";

const App: React.FC = () => {
  return (
    <HashRouter>
      <Switch>
        <Route path="/posts/:collectionName" component={PostsCabinet} />
        <Redirect from="/" to="posts/tests" />
      </Switch>
    </HashRouter>
  );
};

export default App;
